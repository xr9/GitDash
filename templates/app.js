// Get the GitHub username input form
const gitHubForm = document.getElementById('gitHubForm');

// Listen for submissions on GitHub username input form
gitHubForm.addEventListener('submit', (e) => {

    // Prevent default form submission action
    e.preventDefault();

    // Get the GitHub username input field on the DOM
    let usernameInput = document.getElementById('usernameInput');

    // Get the value of the GitHub username input field
    let gitHubUsername = usernameInput.value;

    // Run GitHub API function, passing in the GitHub username
    requestUserRepos(gitHubUsername);

})


function requestUserRepos(username) {

    // Create new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // GitHub endpoint, dynamically passing in specified username
    const url = `https://api.github.com/users/${username}/repos?sort=updated`;

    // Open a new connection, using a GET request via URL endpoint
    // Providing 3 arguments (GET/POST, The URL, Async True/False)
    xhr.open('GET', url, true);

    // When request is received
    // Process it here
    xhr.onload = function() {

        // Parse API data into JSON
        const data = JSON.parse(this.response);
        let root = document.getElementById('userRepos');
        while (root.firstChild) {
            root.removeChild(root.firstChild);
        }
        if (data.message === "Not Found") {
            let ul = document.getElementById('userRepos');

            // Create variable that will create li's to be added to ul
            let li = document.createElement('li');

            // Add Bootstrap list item class to each li
            li.classList.add('list-group-item')
                // Create the html markup for each li
            li.innerHTML = (`
                <p><strong>No account exists with username:</strong> ${username}</p>`);
            // Append each li to the ul
            ul.appendChild(li);
        } else {

            // Get the ul with id of of userRepos
            let ul = document.getElementById('userRepos');
            let p = document.createElement('p');
            
            // ul.appendChild(p);

            let cmp = 0;
            let ref;
            let count = 0;
            // Loop over each object in data array
            for (let i in data) {
                // Create variable that will create li's to be added to ul
                let li = document.createElement('li');

                let rank = data[i].open_issues;
                count = count + rank

                // Add Bootstrap list item class to each li
                li.classList.add('list-group-item')

                // Create the html markup for each li
                li.innerHTML = (`
                <p><strong>Repo Name:</strong> ${data[i].name}</p>
                <p><strong>Description:</strong> ${data[i].description}</p>
                <p><strong>Main Language:</strong> ${data[i].language}</p>
                <p><strong>Open Issues:</strong> ${data[i].open_issues}</p>
                <p><strong>URL:</strong> <a href="${data[i].html_url}">${data[i].html_url}</a></p>
            `);
                if (rank > cmp) {
                    ul.insertBefore(li, ref);
                    cmp = rank;
                    ref = li;
                } else {
                    // Append each li to the ul
                    ul.appendChild(li);
                    ref = ul.firstChild;
                }
                
                

            }
            p.innerHTML = (`
            <p><strong>Number of Public Repos</strong>: ${data.length}</p>
            <p><strong>Total number of open issues</strong>: ${count}</p>`);
            ul.insertBefore(p, ul.firstChild);

        }
    }

    // Send the request to the server
    xhr.send();

}