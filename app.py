import time
from tkinter import messagebox
from github import Github
from tkinter import *
import webbrowser

root = Tk()
root.title('Git Dash')
root.geometry('600x600')
labelCount_issue = 0
labelCount_repo = 0
checkCount_issue = 0
checkCount_repo = 0


def open_web():
    webbrowser.open(f'/templates/index.html')
    # webbrowser.open(f'file://D:/code/UIUC/437/Git Dash/templates/index.html')
    # webbrowser.open(f'file:///home/pi/Desktop/Git Dash/templates/index.html')


def create_issue_label():
    global labelCount_issue

    issueFrameLabel = Label(issueFrame, text='List of top open issues', font=("Courier", 10))
    issueFrameLabel.pack(pady=10)
    labelCount_issue += 1


def create_repo_label():
    global labelCount_repo

    repoFrameLabel = Label(repoFrame, text='List of repository', font=("Courier", 10))
    repoFrameLabel.pack(pady=10)
    labelCount_repo += 1


def check_issue():
    global checkCount_issue
    global issueList
    global scrollbar

    if not labelCount_issue:
        create_issue_label()

    if not checkCount_issue:
        scrollbar = Scrollbar(issueFrame)
        scrollbar.pack(side=RIGHT, fill=Y)
        issueList = Listbox(issueFrame, yscrollcommand=scrollbar.set)
    else:
        issueList.delete(0, END)

    for repo in repos:
        if repo.get_issues().totalCount:
            print(repo.name, repo.description)
            for i in repo.get_issues():
                print(f'title:\t{i.title}\nnumber:\t{i.number}')
                print(i.created_at)
                list_issue.append(i)
                issueList.insert(END,
                                 f'{i.title}; #{i.number}; \"{i.body if i.body else "empty description"}\"')
            # print(repo.get_issue(1).html_url)
    issueList.pack(side=LEFT, expand=TRUE, fill=BOTH)
    scrollbar.config(command=issueList.yview)
    checkCount_issue += 1
    issueList.bind('<Double-1>', issue_select)


def check_repo():
    global checkCount_repo, checkCount_event
    global repoList
    global scrollbar

    if not labelCount_repo:
        create_repo_label()

    if not checkCount_repo:
        scrollbar = Scrollbar(repoFrame)
        scrollbar.pack(side=RIGHT, fill=Y)
        repoList = Listbox(repoFrame, yscrollcommand=scrollbar.set)
    else:
        repoList.delete(0, END)

    for repo in repos:
        list_repo.append(repo)
        print(f'{repo.name}; {repo.language}')
        repoList.insert(END, f'{repo.name}; {repo.language}')

    repoList.pack(side=LEFT, expand=TRUE, fill=BOTH)
    scrollbar.config(command=repoList.yview)
    checkCount_repo += 1
    checkCount_event = 0
    repoList.bind('<Double-1>', repo_select)


def issue_select(event):
    global newWindow

    issue = list_issue[issueList.curselection()[0]]
    print(issue)
    print(issue.user.name)

    newWindow = Toplevel(root)
    newWindow.title('Open Issue')
    newWindow.geometry('400x400')
    Label(newWindow, text='Issue window').pack()
    owner = Label(newWindow, text=f'owner: {issue.user.name}')
    title = Label(newWindow, text=f'title: {issue.title}')
    id = Label(newWindow, text=f'id: {issue.number}')
    # url = Label(newWindow, text=f'url: {issue.html_url}')
    creation = Label(newWindow, text=f'created date: {issue.created_at}')
    update = Label(newWindow, text=f'last updated date: {issue.updated_at}')
    description = Label(newWindow, text=f'Description: {issue.body}')

    btn5 = Button(newWindow, text='open this issue in browser',
                  command=lambda: webbrowser.open_new(f'{issue.html_url}'))

    owner.pack(pady=5)
    title.pack(pady=5)
    id.pack(pady=5)
    # url.pack(pady=5)
    creation.pack(pady=5)
    update.pack(pady=5)
    description.pack(pady=10)
    btn5.pack(pady=10)


def repo_select(event):
    global repoList

    repo = list_repo[repoList.curselection()[0]]

    print(repo)
    print(repo.name)

    webbrowser.open_new(repo.html_url)

    # for e in repo.get_event():


def check_noti():
    noti_count = g.get_user().get_notifications().totalCount
    print(f'notification counts: \t{noti_count}')
    if noti_count:
        messagebox.showinfo("Message", f'You currently have {noti_count} notifications.\nJumping to notification page.')
        # time.sleep(2)
        # webbrowser.open_new('https://github.com/notifications')
    else:
        messagebox.showinfo("Message", f'You have {noti_count} notification.\nRelax.')
        time.sleep(2)
        webbrowser.open_new('https://github.com/notifications')

# replace access_token for privacy
access_token = 'personal access token'
g = Github(access_token)
repos = g.get_user().get_repos()
list_issue = []
list_repo = []

mainFrame = Frame(root)
mainFrame.pack(side=TOP)
mainFrameLabel = Label(mainFrame, text="GitHub Dashboard", font=("Courier", 25))
mainFrameLabel.pack()
issueFrame = Frame(root)
issueFrame.pack(side=LEFT, expand=TRUE, fill=BOTH)
repoFrame = Frame(root)
repoFrame.pack(side=RIGHT, expand=TRUE, fill=BOTH)

btn1 = Button(mainFrame, text='Open browser', command=open_web)
btn1.pack(side=LEFT, padx=20)
btn2 = Button(mainFrame, text='Check open issues', command=check_issue)
btn2.pack(side=LEFT, padx=20)
btn3 = Button(mainFrame, text='Check notifications', command=check_noti)
btn3.pack(side=LEFT, padx=20)
btn4 = Button(mainFrame, text='Check your repos', command=check_repo)
btn4.pack(side=LEFT, padx=20)

root.mainloop()
